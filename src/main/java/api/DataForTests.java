package api;

public class DataForTests {

    //common data
    private static String key = "7b56c77b9f70220c3d5d4ce6477674ea";
    private static int companyId = 213400;

    public static String getKey() {
        return key;
    }

    public static int getCompanyId() {
        return companyId;
    }

    //for getAnyThreeTenderTop test
    private static int setTypeId = 2,
            setId = 1,
            maxRows = 3;

    private static boolean openOnly;

    public static int getTypeId() {
        return setTypeId;
    }

    public static int getId() {
        return setId;
    }

    public static int getMaxRows() {
        return maxRows;
    }

    public static boolean isOpenOnly() {
        return openOnly;
    }

    //for getTenderDiscription test
    private static int tenderId = 301340;


    public static int getTenderId() {
        return tenderId;
    }
}
