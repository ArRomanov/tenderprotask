package api;

import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class ListenerForLogs extends TestListenerAdapter {

    private ByteArrayOutputStream fullLogFromMethod = new ByteArrayOutputStream();

    @Override
    public void onStart(ITestContext testContext) {
        System.setOut(new PrintStream(fullLogFromMethod));
    }

    @Attachment(value = "Full log from method")
    public byte[] logRequest(ByteArrayOutputStream stream) {
        return attach(stream);
    }


    public byte[] attach(ByteArrayOutputStream log) {
        byte[] array = log.toByteArray();
        log.reset();
        return array;
    }

    @Step("Make Attachment on fail")
    @Override
    public void onTestFailure(ITestResult result) {
        logRequest(fullLogFromMethod);
    }

    @Step("Make Attachment on success")
    @Override
    public void onTestSuccess(ITestResult result) {
        logRequest(fullLogFromMethod);
    }
}
