package api;

public class Paths {

    public static final String BASE_URL = "http://www.tender.pro/api/",
            INFO_TENDER_LIST_JSON = "_info.tenderlist_by_set.json",
            INFO_TENDER_JSON = "_tender.info.json",
            INFO_PUBLIC_COMPANY_JSON = "_company.info_public.json";
}
