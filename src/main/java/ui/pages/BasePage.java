package ui.pages;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import org.openqa.selenium.By;

import java.util.concurrent.TimeUnit;

import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public abstract class BasePage {


    public BasePage() {
        baseUrl = "http://www.tender.pro/";
        Configuration.browser = "chrome";
        Configuration.startMaximized = false;
    }

    public abstract BasePage openPage();

    public BasePage clickLMB(String xpath) {
        try {
            TimeUnit.MILLISECONDS.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        $(By.xpath(xpath)).click();
        return this;
    }

    public BasePage checkCountOfObjects(String objectXpath, int count) {
        $$(By.xpath(objectXpath)).shouldHave(CollectionCondition.size(count));

        return this;
    }

    public BasePage assertThatExist(String elementXpath) {
        $(By.xpath(elementXpath)).should(Condition.exist);
        return this;
    }

}
