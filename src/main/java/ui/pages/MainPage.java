package ui.pages;

import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Selenide.open;

public class MainPage extends BasePage {

    private static BasePage MainPage;
    public static final String COMPANIES_LINK = "//a[@id='form_company']";

    private MainPage(){
        super();
    }

    public static BasePage getPage() {
        if (MainPage == null)
            MainPage = new MainPage();
        return MainPage;
    }

    public BasePage openPage() {
        open(baseUrl);
        return this;
    }
}
