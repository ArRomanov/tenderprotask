package ui.pages;

public class RegionCompanyListPage extends BasePage {

    private static BasePage RegionCompanyListPage;
    public final static String PAPER_SWITCH_TOP = "//div[@class='pager'][1]";
    public final static String PAPER_SWITCH_LOW = "//div[@class='pager'][2]";
    public final static String COMPANY_BLOCK_SHORT_DESCRIPTION = "//div[@class='companyDescription']";

    private RegionCompanyListPage() {
        super();
    }

    public static BasePage getPage() {
        if (RegionCompanyListPage == null)
            RegionCompanyListPage = new RegionCompanyListPage();
        return RegionCompanyListPage;
    }

    @Override
    public BasePage openPage() {
        return this;
    }
}
