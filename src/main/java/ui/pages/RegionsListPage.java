package ui.pages;

import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Selenide.open;

public class RegionsListPage extends BasePage {

    private static BasePage CompanyListPage;
    public static final String PENZA_REGION_LINK = "//a[contains(text(),'Пензенская область')]";

    private RegionsListPage() {
        super();
    }

    public static BasePage getPage() {
        if (CompanyListPage == null)
            CompanyListPage = new RegionsListPage();
        return CompanyListPage;
    }

    @Override
    public BasePage openPage() {
        open(baseUrl + "/company_list.shtml");
        return this;
    }

}
