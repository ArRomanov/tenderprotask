package api;

import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static api.DataForTests.*;
import static api.Paths.*;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@Listeners(ListenerForLogs.class)
public class GetMethods {


    @BeforeMethod
    public void prepareTest() {
        RestAssured.registerParser("text/plain", Parser.JSON);
    }

    //check that server return in response 3 tenders with typeId = 2 and id = 1
    @Test
    public void getAnyThreeTenderTop() {
        int rowsCount = getMaxRows();
        int sizeOfArrayItem = 23;
        int statusId = getTypeId();
        int id = getId();
        given()
                .param("_key", getKey())
                .param("set_type_id", statusId)
                .param("set_id", id)
                .param("max_rows", rowsCount)
                .param("open_only", isOpenOnly())
                .log().method().log().uri().log().params()
                .when().get(BASE_URL + INFO_TENDER_LIST_JSON)
                .then().log().cookies().log().body()
                .statusCode(200)                                             //check status of response code
                .assertThat()
                .body("success", equalTo("true"))              //check success server's answer
                .body("result.data[1].type_id", equalTo(id))              //check id
                .body("result.data[2].status_id", equalTo(statusId))      //check statusId
                .body("result.data.size()", is(rowsCount))              //check size of object "data"
                .body("result.data[1].size()", is(sizeOfArrayItem));    //check size of one item of json array
    }

    //test for check tender id and count line in object "data"
    @Test
    public void getTenderDiscription() {
        int tenderId = getTenderId();
        given()
                .param("_key", getKey())
                .param("company_id", getCompanyId())
                .param("id", tenderId)
                .log().method().log().uri().log().params()
                .when().get(BASE_URL + INFO_TENDER_JSON)
                .then().log().cookies().log().body()
                .statusCode(200)                                                //check status of response code
                .assertThat()
                .body("success", equalTo("true"))                 //check success server's answer
                .body("result.data.id", equalTo(tenderId))                 //check of tender id from response
                .body("result.data.size()", is(19));                 //check size of object "data"
    }

    //test for check info of company (id) and count of line in object "data"
    @Test
    public void getCompanyDetails() {
        int companyId = getCompanyId();
        given()
                .param("id", companyId)
                .log().method().log().uri().log().params()
                .when()
                .get(BASE_URL + INFO_PUBLIC_COMPANY_JSON)
                .then()
                .log().cookies().log().body()
                .statusCode(200)
                .assertThat()
                .body("success", equalTo("true"))                 //check success server's answer
                .body("result.data.id", equalTo(companyId))                //check of tender id from response
                .body("result.data.size()", is(19));                 //check size of object "data"
    }
}
