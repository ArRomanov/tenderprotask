package ui;

import org.testng.annotations.Test;
import ui.pages.MainPage;

import static ui.pages.MainPage.COMPANIES_LINK;
import static ui.pages.RegionCompanyListPage.*;
import static ui.pages.RegionsListPage.PENZA_REGION_LINK;

public class NavigateOnPages {

    //проверяем количество блоков(компаний) на странице региона, проверка наличия верхнего и нижнего переключателя страниц
    @Test
    public void checkCountCompaniesOnPage() {
        MainPage
                .getPage()
                .openPage()
                .clickLMB(COMPANIES_LINK)
                .clickLMB(PENZA_REGION_LINK)
                .assertThatExist(PAPER_SWITCH_TOP)
                .assertThatExist(PAPER_SWITCH_LOW)
                .checkCountOfObjects(COMPANY_BLOCK_SHORT_DESCRIPTION, 50);
    }
}
